#pragma once
#include "model.hpp"
#include "camera.hpp"

void loadScene(Model& model, Camera& cam, const std::string& name = "default");