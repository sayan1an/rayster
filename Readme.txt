Visual Studio->Properties->general->c++ language standart c++17

Include folders

D:\projects\VkExperiment\external\glfw-3.2.1.bin.WIN64\include
D:\projects\VkExperiment\external\imgui
D:\projects\VkExperiment\external\implot
D:\vcpkg\installed\x86-windows\include // this is for assimp binaries
D:\projects\VkExperiment\external\tinyobjloader
D:\projects\VkExperiment\external\stb
D:\projects\VkExperiment\external\glm
D:\projects\VkExperiment\external\spline\src
D:\projects\VkExperiment\external\cereal\include
C:\VulkanSDK\1.1.106.0\Include
D:\projects\VkExperiment\external\VulkanMemoryAllocator\src

Lib folders

D:\vcpkg\installed\x86-windows\lib
D:\projects\VkExperiment\external\glfw-3.2.1.bin.WIN64\lib-vc2015
C:\VulkanSDK\1.1.106.0\Lib

Lib files

glfw3.lib
vulkan-1.lib
assimp-vc142-mt.lib

Note for imgui and implot

Add all imgui/implot files in your project