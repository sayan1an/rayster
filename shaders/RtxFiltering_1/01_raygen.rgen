#version 460
#extension GL_NV_ray_tracing : require
#extension GL_GOOGLE_include_directive : enable
#include "../commonMath.h"

layout(binding = 0, set = 0) uniform accelerationStructureNV topLevelAS;
layout(binding = 1, set = 0, rgba8) uniform readonly image2D diffuseImage;
layout(binding = 2, set = 0, rgba8) uniform readonly image2D specularImage;
layout(binding = 3, set = 0, rgba32f) uniform readonly image2D normalImage;
layout(binding = 4, set = 0, rgba32f) uniform readonly image2D otherImage;
layout(binding = 5, set = 0, rgba32f) uniform image2D outImage;
layout(binding = 6, set = 0) uniform CameraProperties 
{
	mat4 view;
    mat4 proj;
    mat4 viewInv;
    mat4 projInv;
} cam;

layout(binding = 7, set = 0) readonly buffer LightVertices { vec4 v[]; } lightVertices;
layout(binding = 8, set = 0) readonly buffer DiscretePdf { float bins[]; } discretePdf;
layout(binding = 9, set = 0) readonly buffer RandomSquareSamples { vec2 v[]; } randSqSamples;

layout (push_constant) uniform pcBlock {
	float power;
	uint discretePdfSize;
	uint numSamples;
} pcb;

layout(location = 0) rayPayloadNV uint hit;
layout(location = 1) rayPayloadNV vec3 radiance;

uint xorshiftState;

vec3 shadowRayAreaLight(vec3 lightPosition, vec3 viewDir, vec3 origin, vec4 surfaceNormal, float bsdfType, vec3 diffRef, vec3 specRef)
{	
	float distance = length(lightPosition - origin);
	vec3 lightDir = (lightPosition - origin) / distance;
	
	float cos = dot(lightDir, surfaceNormal.xyz);	
	
	if (cos < 0)
		return vec3(0, 0, 0);

	radiance = -lightDir;
	traceNV(topLevelAS, gl_RayFlagsOpaqueNV, gl_RayFlagsTerminateOnFirstHitNV | gl_RayFlagsOpaqueNV
		, 1, 0, 1, origin, 0.01f, lightDir, 10000.0f, 1);

	vec3 specularColor = bsdfType > 0.5f ? specRef *  ggxBrdf(viewDir, lightDir, surfaceNormal.xyz, surfaceNormal.w) : vec3(0, 0, 0);
	vec3 diffuseColor = diffRef * cos / PI;
		
	return (diffuseColor + specularColor) * radiance  / (distance * distance);
}

vec2 uvToBary(in uint index)
{	
	vec2 bary = randSqSamples.v[index];
	bary.x = 1.0 - sqrt(bary.x);
	bary.y = (1.0 - bary.x) * (1.0 - bary.y);

	return bary;
}

void main()
{	
	const ivec2 pixel = ivec2(gl_LaunchIDNV.xy);
	const vec4 diffuseColor = imageLoad(diffuseImage, pixel).rgba;
	const vec4 other = imageLoad(otherImage, pixel).rgba;

	vec4 color = vec4(0, 0, 0, 1);
	// If the primary intersection is a light source
	if (other.w > 3.5 && other.w < 4.5)
		color = diffuseColor;
	// primary hit point is valid
	else if (other.w > -0.5f){
		const vec4 specularColor = imageLoad(specularImage, pixel).rgba;
		const vec4 normal = imageLoad(normalImage, pixel).rgba;

		const vec2 pixelCenter = vec2(gl_LaunchIDNV.xy) + vec2(0.5);
		const vec2 inUV = pixelCenter/vec2(gl_LaunchSizeNV.xy);
		vec2 d = inUV * 2.0 - 1.0;

		vec4 origin = cam.viewInv * vec4(0,0,0,1);
		vec4 target = cam.projInv * vec4(d.x, d.y, 1, 1) ;
		vec4 viewDir = cam.viewInv * vec4(normalize(target.xyz), 0) ;
	
		// for shadow rays
		origin = origin + (other.x - 0.000f) * viewDir;
			
		for (uint i = 0; i < pcb.numSamples; i++) {
			vec2 bary = uvToBary(i); 
			uint index = i & 1;
			
			color.xyz += shadowRayAreaLight((lightVertices.v[3*index] * bary.x + lightVertices.v[3*index + 1] * bary.y + lightVertices.v[3*index + 2] * (1 - bary.x - bary.y)).xyz,
				-viewDir.xyz, origin.xyz, normal, other.w, diffuseColor.xyz, specularColor.xyz) / pcb.numSamples;

		}
		
		color *= pcb.power;
	}

	imageStore(outImage, pixel, color);
}
